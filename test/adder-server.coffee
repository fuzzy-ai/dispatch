# adder-server.coffee -- server for dispatching jobs to job handlers
# Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

_ = require 'lodash'
fs = require 'fs'
path = require 'path'
debug = require('debug')('dispatch:adder-server')

Microservice = require 'fuzzy.ai-microservice'

class AdderServer extends Microservice

  getName: -> "adder"

  setupRoutes: (exp) ->

    exp.post '/add', @appAuthc, @add

    exp

  add: (req, res, next) ->
    if !_.isObject(req.body)
      next new Error("/add requires JSON: {augend: <number>, addend: <number>}")
    else
      debug(req.body)
      augend = req.body.augend
      addend = req.body.addend
      if !_.isFinite(augend)
        next new Error("augend is not a finite number: #{augend}")
      else if !_.isFinite(addend)
        next new Error("addend is not a finite number: #{addend}")
      else
        showResult = ->
          result = sum: augend + addend
          debug result
          res.json result
        setTimeout showResult, 200

  startDatabase: (callback) -> callback null
  stopDatabase: (callback) -> callback null

module.exports = AdderServer
