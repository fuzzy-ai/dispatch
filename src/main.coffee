# main.coffee
# Copyright 2014-2016 9165584 Canada Corporation <legal@fuzzy.ai>
# All rights reserved.

DispatchServer = require './dispatch-server'

# XXX: send a message to Slack for uncaught exceptions

process.on 'uncaughtException', (err) ->
  console.log "******************** UNCAUGHT EXCEPTION ********************"
  console.error err
  if err.stack
    console.dir err.stack.split("\n")
  process.exit 127

# No errors on too many listeners

process.setMaxListeners 0

server = new DispatchServer process.env

server.start (err) ->
  if err
    console.error(err)
  else
    console.log("Server listening")

# Try to send a slack message on error

process.on 'uncaughtException', (err) ->
  if server? and server.slackMessage?
    msg = "UNCAUGHT EXCEPTION: #{err.message}"
    server.slackMessage "error", msg, ":bomb:", (err) ->
      if err?
        console.error err

shutdown = ->
  console.log "Shutting down..."
  server.stop (err) ->
    if (err)
      console.error err
      process.exit -1
    else
      console.log "Done."
      process.exit 0

process.on 'SIGTERM', shutdown
process.on 'SIGINT', shutdown
