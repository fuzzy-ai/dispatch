fs = require 'fs'

{spawn} = require 'child_process'

glob = require 'glob'

cmd = (str, callback) ->
  parts = str.split(' ')
  main = parts[0]
  rest = parts.slice(1)
  proc = spawn main, rest
  proc.stderr.on 'data', (data) ->
    process.stderr.write data.toString()
  proc.stdout.on 'data', (data) ->
    process.stdout.write data.toString()
  proc.on 'exit', (code) ->
    if code is 0
      callback?()
    else
      process.exit(code)

build = (callback) ->
  cmd 'coffee -c -o lib src', callback
  
buildDocker = (callback) ->
  cmd 'sudo docker build -t fuzzyio/dispatcher .', callback

buildTest = (callback) ->
  cmd 'coffee -c test', callback

task 'build', 'Build lib/ from src/', ->
  build()

task "build-test", "Build for testing", ->
  invoke "clean"
  invoke "build"
  buildTest()

clean = (callback) ->
  patterns = [
    "lib/*.js"
      "test/*.js"
    "*~"
    "lib/*~"
    "src/*~"
    "test/*~"]
  for pattern in patterns
    glob pattern, (err, files) ->
      for file in files
        fs.unlinkSync file
  callback?()

task 'clean', 'Clean up extra files', ->
  clean()

option '-t', '--test [NAME]', 'specific test to run'

task "test", "Test the API", (options) ->
  clean ->
    build ->
      buildTest ->
        if options.test?
          cmd "./node_modules/.bin/perjury test/#{options.test}-test.js"
        else
          glob "test/*-test.js", (err, files) ->
            if err
              console.error err
            else
              cmd "./node_modules/.bin/perjury #{files.join(' ')}"

task 'docker', 'Build docker image', ->
  invoke 'clean'
  buildDocker()

task 'push', 'Push to Docker hub', ->
  cmd 'sudo docker push fuzzyio/dispatcher'
