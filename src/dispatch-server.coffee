# dispatch-server.coffee -- server for dispatching jobs to job handlers
# Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

fs = require 'fs'
path = require 'path'
debug = require('debug')('dispatch:server')
async = require 'async'
_ = require 'lodash'
{WebClient} = require 'fuzzy.ai-web'
Microservice = require 'fuzzy.ai-microservice'
PriorityQueue = require 'js-priority-queue'

class DispatchServer extends Microservice

  getName: -> "dispatch"

  environmentToConfig: (env) ->

    cfg = super env

    cfg.webClientTimeout = @envInt env, "WEB_CLIENT_TIMEOUT", 5
    if env.WORKERS
      cfg.workers = env.WORKERS.split(',')
    else
      cfg.workers = []
    cfg.maxRetries =
      @envInt env, "MAX_RETRIES", Math.max(cfg.workers.length, 16)

    cfg

  setupExpress: ->

    exp = super()

    @webClient = new WebClient timeout: 0

    # Priority queue of workers so that flaky workers are called less often

    @available = new PriorityQueue comparator: (a, b) ->
      b.successRate - a.successRate

    for worker in @config.workers
      @available.queue {worker: worker, successRate: 1, count: 0}

    @queue = async.queue @handleRequest, Math.max(@config.workers.length, 1)

    exp

  handleRequest: (task, callback) =>
    {endpoint, payload, token, retries} = task
    worker = null
    successRate = null
    count = null
    async.waterfall [
      (callback) =>
        if @available.length == 0
          callback new Error("No more workers available")
        else
          {worker, successRate, count} = @available.dequeue()
          debug("Got worker #{worker} from pool: #{successRate} on #{count}")
          callback null, worker
      (results, callback) =>
        worker = results
        url = "#{worker}/#{endpoint}"
        body = JSON.stringify(payload)
        debug "sending #{body} to #{worker}"
        headers =
          "Content-Type": "application/json; charset=utf-8"
          "Content-Length": Buffer.byteLength(body, "utf-8")
          "Authorization": "Bearer #{token}"
        debug url
        debug body
        debug headers
        @webClient.post url, headers, body, callback
      (response, results, callback) ->
        callback null, JSON.parse(results)
    ], (err, body) =>
      if worker?
        debug("Returning #{worker} to pool")
        if !err?
          successRate = ((successRate * count) + 1)/(count + 1)
        else
          successRate = (successRate * count)/(count + 1)
        count = count + 1
        @available.queue
          worker: worker
          successRate: successRate
          count: count
        debug("available workers = #{@available.length}")
      if err
        debug("Error for task with #{JSON.stringify(payload)}: #{err}")
        debug("Error body: #{err.body}")
        debug(err.stack)
        @log.warn
          err: err
          worker: worker
          endpoint: endpoint
          token: token
          payload: payload
          retries: retries
          message: "Error for task"
        # XXX: exponential backoff
        callback err
      else
        debug("Got results for #{JSON.stringify(payload)}")
        debug(body)
        callback null, body

  setupRoutes: (exp) ->

    exp.get '/version', @version
    exp.post '/:endpoint', @reqToken, @endpoint

    exp

  reqToken: (req, res, next) =>
    @bearerToken req, (err, tokenString) ->
      if err
        next err
      else
        req.token = tokenString
        next()

  endpoint: (req, res, next) =>

    name = req.params.endpoint

    if !name?
      return next new Error("No queue name")

    if !req.body?
      return next new Error("No job data")

    task =
      endpoint: name
      token: req.token
      payload: req.body
      retries: 0

    debug(task)

    # This needs to refer to itself for the retries

    handleTaskResults = (err, results) =>
      if err
        debug("task #{task.endpoint} ended with error")
        if task.retries < @config.maxRetries
          debug("retries = #{task.retries} (max = #{@config.maxRetries}), OK")
          task.retries += 1
          @queue.push task, handleTaskResults
        else
          debug("retries = #{task.retries} (max = #{@config.maxRetries}), :-p")
          next err
      else
        res.json results

    @queue.push task, handleTaskResults

  version: (req, res, next) =>

    @getVersion (err, version) =>
      if err
        next err
      else
        res.json
          name: @getName()
          version: version

  startDatabase: (callback) -> callback null
  stopDatabase: (callback) -> callback null

  stopCustom: (callback) ->
    debug("Stopping with custom stuff")
    async.waterfall [
      (callback) =>
        debug("Stopping web client")
        @webClient.stop()
        callback null
      (callback) =>
        if @queue.length() == 0
          debug("queue length is zero; not draining")
          callback null
        else
          debug("draining queue")
          @queue.drain = ->
            debug("done draining queue")
            callback null
    ], callback

  getVersion: (callback) ->

    if @savedVersion?
      callback null, @savedVersion
    else
      fs.readFile path.join(__dirname, '..', 'package.json'), (err, pkg) =>
        if err
          callback err
        else
          data = JSON.parse pkg
          @savedVersion = data.version
          callback null, @savedVersion

module.exports = DispatchServer
