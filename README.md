dispatcher
==========

This is a reverse-proxy for microservices that might take a long time to run.
haproxy was working poorly for us, so this is an attempt to make something
that works better.

It works like this: it accepts requests at an endpoint on the front end, like
"/evaluate". It takes the JSON content of that request and sticks it in a queue.
When the queue job is run, it picks a back-end server to send the request to,
and sends it -- passing through the JSON and the OAuth2 token.

If there is an error, it will requeue the job, up to a maximum (see below). If
it succeeds, it just returns the results to the client.

Environment variables
---------------------

It's a (microservice)(https://github.com/fuzzy-ai/microservice), so it takes the
same env variables that other microservices do. In addition, it uses the
following:

- WEB_CLIENT_TIMEOUT: How long to leave connections open in the WebClient.
  Note that this is currently being ignored and connections are closed for
  each request. Defaults to 5 seconds.
- WORKERS: a comma-separated list of root URLs for back-end servers. Example:
  'http://evaluator-1,http://evaluator-2:8000'. Default is no back-end
  servers, which is probably not what you want.
- MAX_RETRIES: Maximum number of times to retry a request if there are errors
  from the back end. Defaults to either 16, or the number of workers, whichever
  is larger.
