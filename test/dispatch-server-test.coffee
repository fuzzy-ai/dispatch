# dispatch-server-test.coffee -- test starting and stopping a dispatch server
# Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

vows = require 'perjury'
assert = vows.assert
web = require 'fuzzy.ai-web'

env = require './env'

vows.describe 'DispatchServer'
  .addBatch
    'When we load the DispatchServer class':
      topic: ->
        require '../lib/dispatch-server'
      'it works': (err, DispatchServer) ->
        assert.ifError err
      'it is a function': (err, DispatchServer) ->
        assert.ifError err
        assert.isFunction DispatchServer
      'and we create an instance':
        topic: (DispatchServer) ->
          new DispatchServer env
        'it works': (err, dispatch) ->
          assert.ifError err
        'it is an object': (err, dispatch) ->
          assert.ifError err
          assert.isObject dispatch
        'it has a start() method': (err, dispatch) ->
          assert.ifError err
          assert.isObject dispatch
          assert.isFunction dispatch.start
        'it has a stop() method': (err, dispatch) ->
          assert.ifError err
          assert.isObject dispatch
          assert.isFunction dispatch.stop
        'and we start the server':
          topic: (dispatch) ->
            dispatch.start @callback
            undefined
          'it works': (err) ->
            assert.ifError err
          'and we request the version':
            topic: ->
              web.get "http://localhost:1516/version", (err, request, body) =>
                if err
                  @callback err
                else
                  @callback null, JSON.parse(body)
              undefined
            'it works': (err, body) ->
              assert.ifError err
              assert.isObject body
              assert.isString body.name, "body.name #{body.name} is not a string"
              assert.equal body.name, "dispatch"
              assert.isString body.version, "body.version #{body.version} is not a string"
            'and we stop the server':
              topic: (body, dispatch) ->
                dispatch.stop @callback
                undefined
              'it works': (err) ->
                assert.ifError err
              'and we request the version':
                topic: ->
                  web.get "http://localhost:1516/version", (err, request, body) =>
                    if err
                      @callback null
                    else
                      @callback new Error("Unexpected success")
                  undefined
                'it fails correctly': (err) ->
                  assert.ifError err
  .export module
