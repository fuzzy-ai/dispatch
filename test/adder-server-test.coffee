# dispatch-server-test.coffee -- test starting and stopping a dispatch server
# Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

vows = require 'perjury'
assert = vows.assert
web = require 'fuzzy.ai-web'
debug = require('debug')('dispatch:adder-server-test')
async = require 'async'

MAX_NUMBER = 16
CONCURRENCY = 128

env = require './adder-env'

vows.describe 'AdderServer'
  .addBatch
    'When we load the AdderServer class':
      topic: ->
        require './adder-server'
      'it works': (err, AdderServer) ->
        assert.ifError err
      'it is a function': (err, AdderServer) ->
        assert.ifError err
        assert.isFunction AdderServer
      'and we create an instance':
        topic: (AdderServer) ->
          new AdderServer env
        'it works': (err, dispatch) ->
          assert.ifError err
        'it is an object': (err, dispatch) ->
          assert.ifError err
          assert.isObject dispatch
        'it has a start() method': (err, dispatch) ->
          assert.ifError err
          assert.isObject dispatch
          assert.isFunction dispatch.start
        'it has a stop() method': (err, dispatch) ->
          assert.ifError err
          assert.isObject dispatch
          assert.isFunction dispatch.stop
        'teardown': (server) ->
          if server? and server.stop?
            server.stop @callback
          else
            @callback null
          undefined
        'and we start the server':
          topic: (dispatch) ->
            dispatch.start @callback
            undefined
          'it works': (err) ->
            assert.ifError err
          'and we add a bunch of numbers':
            topic: ->
              client = new web.WebClient({timeout: 5000})
              addNumbers = (pair, callback) ->
                url = "http://localhost:2342/add"
                props =
                  augend: pair[0]
                  addend: pair[1]
                payload = JSON.stringify(props)
                debug payload
                headers =
                  "Content-Type": "application/json; charset=utf-8"
                  "Content-Length": Buffer.byteLength(payload, "utf-8")
                  "Authorization": "Bearer #{env.APP_KEY_UNIT_TEST}"
                client.post url, headers, payload, (err, request, body) ->
                  if err
                    debug(err.body)
                    callback err
                  else
                    debug(body)
                    callback null, JSON.parse(body)
              pairs = []
              for i in [0..MAX_NUMBER-1]
                for j in [0..MAX_NUMBER-1]
                  pairs.push [i, j]
              async.mapLimit pairs, CONCURRENCY, addNumbers, (err, bodies) =>
                client.stop()
                @callback err, bodies
              undefined
            'it works': (err, bodies) ->
              assert.ifError err
              assert.isArray bodies
              assert.lengthOf bodies, MAX_NUMBER * MAX_NUMBER
              for body in bodies
                assert.isObject body
                assert.isNumber body.sum, "body.sum #{body.sum} is not a number"
  .export module
