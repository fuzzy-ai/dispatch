# dispatch-test.coffee -- Test basic dispatch processing
# Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>
# All rights reserved.

vows = require 'perjury'
assert = vows.assert
_ = require 'lodash'
async = require 'async'
debug = require('debug')('dispatch:dispatch-scale-test')
web = require('fuzzy.ai-web')

env = require './env'
adderEnv = require './adder-env'

_.assign adderEnv,
  APP_KEY_CLIENT: "boris-faa-vain-base-cosh"
  DISPATCH_ROOT: "http://localhost:1516"
  DISPATCH_CHANNEL: "add"

MAX_ADDERS = 16
MAX_NUMBER = 32
CONCURRENCY = 16

vows.describe 'Dispatch processing at scale'
  .addBatch
    'When we start a few adder servers':
      topic: (dispatch) ->
        AdderServer = require './adder-server'
        startAdder = (i, callback) ->
          myEnv = _.cloneDeep adderEnv
          mainPort = parseInt(adderEnv.PORT, 10)
          myPort = mainPort + i
          myEnv.PORT = "#{myPort}"
          adder = new AdderServer myEnv
          adder.start (err) ->
            if err
              callback err
            else
              callback null, adder
        async.times MAX_ADDERS, startAdder, @callback
        undefined
      'it works': (err, adders) ->
        assert.ifError err
        assert.isArray adders
        for adder in adders
          assert.isObject adder
      teardown: (adders) ->
        stopAdder = (adder, callback) ->
          debug("stopping adder on port #{adder.config.port}")
          adder.stop (err) ->
            debug("done stopping adder on port #{adder.config.port}")
            callback err
        async.each adders, stopAdder, @callback
        undefined
      'and we start a dispatch server':
        topic: (adders) ->
          addresses = _.map adders, (adder) ->
            "http://#{adder.config.hostname}:#{adder.config.port}"
          newEnv = _.assign {WORKERS: addresses.join(',')}, env
          DispatchServer = require '../lib/dispatch-server'
          dispatch = new DispatchServer newEnv
          dispatch.start (err) =>
            if err
              @callback err
            else
              @callback null, dispatch
          undefined
        'it works': (err, dispatch) ->
          assert.ifError err
          assert.isObject dispatch
        teardown: (dispatch) ->
          debug("Tearing down dispatch")
          if dispatch? and dispatch.stop?
            dispatch.stop (err) =>
              debug "Done tearing down dispatch"
              @callback null
          else
            @callback null
          undefined
        'and we add a lot of numbers':
          topic: ->
            addNumbers = (pair, callback) ->
              url = "http://localhost:1516/add"
              props =
                augend: pair[0]
                addend: pair[1]
              payload = JSON.stringify(props)
              debug payload
              headers =
                "Content-Type": "application/json; charset=utf-8"
                "Content-Length": Buffer.byteLength(payload, "utf-8")
                "Authorization": "Bearer #{adderEnv.APP_KEY_CLIENT}"
              web.post url, headers, payload, (err, request, body) ->
                if err
                  debug("Got error for #{pair}")
                  debug(err)
                  callback err
                else
                  debug("Got results for #{pair}")
                  debug(body)
                  callback null, JSON.parse(body)

            pairs = []
            for i in [0..MAX_NUMBER - 1]
              for j in [0..MAX_NUMBER - 1]
                pairs.push [i, j]

            async.mapLimit pairs, CONCURRENCY, addNumbers, (err, bodies) =>
              debug("Done with the number adding")
              if err
                @callback err
              else
                @callback null, bodies

            undefined

          'it works': (err, bodies) ->
            assert.ifError err
            assert.isArray bodies
            assert.lengthOf bodies, MAX_NUMBER * MAX_NUMBER
            for body in bodies
              assert.isObject body
              assert.isNumber body.sum, "body.sum #{body.sum} is not a number"

  .export module
